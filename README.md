# LyricsSoup

Fetch all the lyrics from a band / artist in lyrics.wikia.com using Python3 and BeautifulSoup. Lyrics file tree will be following:
path_to_code/lyrics/artist/album/01 - song name.txt
or if album contains multiple CD's:
path_to_code/lyrics/artist/album/CD1 - 01 - song name.txt

Code will need tune in couple of weeks as wikia.com migrates to fandom.com and some changes will occur.

Change variable BAND to wanted band / artist on same format as on wikia page. If artist page url is:
lyrics.wikia.com/wiki/CMX
Set variable to 'CMX'.

## Requirements

Python3, BeautifulSoup4 and lxml. Install latest or use included requirements.txt.

Step by step:

```
1. Cd to code direction
2. Change band name / URL in code (optional :))
3. Make and activate virtual environment
4. Install requirements ( pip install -r requirements.txt )
5. Execute with: python3 -m lyricsSoup
```
