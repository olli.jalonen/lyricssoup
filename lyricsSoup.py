# Imports
from lxml import etree
from bs4 import BeautifulSoup, NavigableString
import urllib.request
import os
import time

BAND = 'CMX'
BASEURL = 'http://lyrics.wikia.com'
FRONTURL = BASEURL + '/wiki/' + BAND
PATH = os.getcwd() + '/lyrics/' + BAND + '/'

# Wait time before sending a new request
WAITTIME = 3

# Replace element br tags with \n, returns text
def replaceWithNewlines(element):
    text = ''
    for elem in element.recursiveChildGenerator():
        if isinstance(elem, str):
            text += elem.strip()
        elif elem.name == 'br':
            text += '\n'
    return text

# Get album names and urls for album specific song pages, returns dictionary
def getAlbums():
    temp = {}
    source = urllib.request.urlopen(FRONTURL).read()
    soup = BeautifulSoup(source, 'lxml')
    for li in soup.findAll('span', {'class': 'mw-headline'}):
        if hasattr(li.a, 'text'):
            temp[li.a.text] = li.a.get('href')
    return temp

# Get songs
def getSongs(albums):
    temp = {}
    for album, albumUrl in albums.items():
        # Fetch song urls
        print('Fetch song urls from {} url: {}'.format(album, BASEURL + albumUrl))
        time.sleep(WAITTIME)  # wait given second before making the next request
        alSource = urllib.request.urlopen(BASEURL + albumUrl).read()
        alSoup = BeautifulSoup(alSource, 'lxml')

        # Loop songs
        temp[album] = {}
        cd = 0
        for ol in alSoup.findAll('ol'):
            cd += 1
            temp[album][cd] = {}
            for li in ol: 
                if hasattr(li.a, 'text'):
                    temp[album][cd][li.a.text] = li.a.get('href')
                    # print('CD: {} Album: {} url: {}'.format(cd, li.a.text, li.a.get('href')))
    return temp

def saveSong(songPath, songUrl):
    # Create text file
    songFile = open(songPath, 'w+')

    # Open song url
    time.sleep(WAITTIME)  # wait given seconds before making the next request
    songSource = urllib.request.urlopen(songUrl).read()
    songSoup = BeautifulSoup(songSource, 'lxml')

    # Get text
    songText = replaceWithNewlines(songSoup.find('div', {'class': 'lyricbox'}))

    # Save text
    songFile.write(songText)

    # Close text file
    songFile.close()

    return

# Get albums
albums = getAlbums()

# Get all songs
songs = getSongs(albums)

# Create lyrics dir with band name if not exists
if not os.path.exists(PATH):
    os.makedirs(PATH)

# Loop albums
for album, albumUrl in albums.items():
    # Create dir with album name if it does not exist
    if not os.path.exists(PATH + album):
        os.makedirs(PATH + album)
    
    # Check if album contains multiple CDs and needs CDx prefix
    cdPrefix = False
    if len(songs[album]) > 1:
        cdPrefix = True
    
    # Loop album songs
    for cd in songs[album]:
        # Start song numbering from 1 (every CD)
        songCounter = 1
        for song, songUrl in songs[album][cd].items():
            print('Fetch song {} - {} from url: {}'.format(str(songCounter), song, BASEURL + songUrl))
            # Save song (file path, url)
            if cdPrefix:
                saveSong(PATH + album + '/CD' + str(cd) + ' - ' + str(songCounter) + ' - ' + song + '.txt', BASEURL + songUrl)
            else:
                saveSong(PATH + album + '/' + str(songCounter) + ' - ' + song + '.txt', BASEURL + songUrl)
            songCounter += 1
